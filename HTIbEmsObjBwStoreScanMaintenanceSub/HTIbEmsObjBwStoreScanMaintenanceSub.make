RPM_SRC_DIR=/opt/tibco/rpmbuild/SOURCES
VERSION=1.0
APP=HTIbEmsObjBwStoreScanMaintenanceSub
APPDIR=/opt/tibco
USER=tibco
GROUP=tibco
# Be sure to change all above fields to suit your needs!
# Nothing from here down should need changing.

default:
	echo "no default rule yet."

distsrc:
   # Start with an empty source directory to contain the entire source tree
	# in the tarball.  Note that we are basically copying the entire
	# APPDIR tree into this new empty source directory in order to tar it up
	# for use as the source for the RPM build.
	# Thus the filelist should be built relative to the APPDIR.

	rm -rf $(APP)-$(VERSION) > /dev/null 2>&1
	mkdir -p $(APP)-$(VERSION)$(APPDIR)

	# genspec.sh will create the RPM-ready spec filelist based on the input
	# from $(APP)_deploy.filelist.
	# The specfilelist must end up in the source tarball to be found by RPM later

	genspec.sh $(APP) $(APPDIR) $(USER) $(GROUP)
	cp $(APP).specfilelist $(APP)-$(VERSION)  # Needed for RPM build

	# Grab all the specified files, tarring them and sending them through
	# a pipe which untars them into the new temporary location

	tar --no-recursion -cf - -T $(APP)_source.filelist | \
	    (cd $(APP)-$(VERSION)$(APPDIR); tar -xf -)

	# Now create a NEW tar file containing the payload correctly organized
	# in the new source tree
	tar -c $(APP)-$(VERSION) | gzip -c > $(RPM_SRC_DIR)/$(APP)-$(VERSION).tar.gz
	rm -rf $(APP)-$(VERSION)

dist:
	rpmbuild -bb $(APP).spec

