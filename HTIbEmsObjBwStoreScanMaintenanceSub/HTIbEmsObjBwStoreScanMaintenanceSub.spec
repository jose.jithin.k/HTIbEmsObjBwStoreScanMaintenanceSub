Summary:  H-E-B TIBCO EMS Objects for StoreScanMaintenanceSubscriber Integration
Name: HTIbEmsObjBwStoreScanMaintenanceSub
Version: 1.0
Release: 1
License: HEB
Group: Applications/Integration
Distribution: RedHat
Packager: ICC Team
Source: %{name}-%{version}.tar.gz
BuildRoot: %{_topdir}/BUILD/%{name}-%{version}
Requires: HTIbEmsAddJms, HTIbEmsStrt
%define _unpackaged_files_terminate_build 0

%description
H-E-B TIBCO EMS Objects for StoreScanMaintenanceSubscriber Integration

%prep
# The only thing that should be changed in this section is the second
# path.  This logic just keeps you from setting RPM_BUILD_ROOT to be the
# actual home directory or the root directory, thus preventing you from wiping
# out your entire directory tree.  This variable is set from the BuildRoot
# field above.  Don't change it!  Only change the name and version fields.
# This forces the build directory to be created and packaged within your 
# rpmbuild/BUILD area.
if [ $RPM_BUILD_ROOT != '/' ] && [ $RPM_BUILD_ROOT != '/opt/tibco/' ]
then
  rm -rf $RPM_BUILD_ROOT
  mkdir $RPM_BUILD_ROOT
else
  echo "Bad build root!!!"
  exit 1
fi

%setup
# not needed

%build
# not needed

%install
cp -r %{_builddir}/%{name}-%{version}/* ${RPM_BUILD_ROOT}

%pre

%post
HOME=/opt/tibco
RPM_HOME=${HOME}/rpm
AUDITS=${RPM_HOME}/audits
FILES=${RPM_HOME}/files
SCRIPT=${RPM_HOME}/script
BIN=${RPM_HOME}/bin
LOG=${AUDITS}/rpm.log
TAR=/bin/tar
RC=0

echo "`date` INFO: Starting install package %{name} " >> ${LOG} 2>&1

touch ${AUDITS}/%{name}.log

echo "`date` INFO: unpacking package %{name}.tar.gz" >> ${AUDITS}/%{name}.log 2>&1

${TAR} -C ${HOME} -zxf ${HOME}/%{name}.tar.gz
if [ $? -ne 0 ]; then
   echo "`date` ERROR: ${HOME}/%{name}.tar.gz untar failed" >> ${AUDITS}/%{name}.log 2>&1
else
   echo "`date` INFO: ${HOME}/%{name}.tar.gz untar successful" >> ${AUDITS}/%{name}.log 2>&1
fi

## install script
su - tibco -c "${SCRIPT}/create_ems_objects.sh" >> ${AUDITS}/%{name}.log 2>&1
RC=$?
if [ ${RC} -ne 0 ]; then
   echo "`date` ERROR: package %{name} installation failed" >> ${LOG} 2>&1
else
   echo "`date` INFO: package %{name} installed successfully" >> ${LOG} 2>&1
fi

chown tibco:tibco ${LOG}
chmod 755 ${LOG}

chown tibco:tibco ${AUDITS}/%{name}.log
chmod 755 ${AUDITS}/%{name}.log

## Cleanup
## remove your temp directories for future use
rm -fR ${SCRIPT}
rm -fR ${FILES}
rm -fR ${BIN}
## don't remove the audits dir

echo "`date` INFO: removing package ${HOME}/%{name}.tar.gz" >> ${AUDITS}/%{name}.log 2>&1

rm ${HOME}/%{name}.tar.gz

echo "`date` INFO: Finished install package %{name} " >> ${LOG} 2>&1

exit ${RC}

%preun
# not needed

%postun 
# not needed

%files
%attr(755,tibco,tibco) /opt/tibco/%{name}.tar.gz


%clean
# not needed

%changelog
* Fri Mar 10 2021 Jithin K Jose (vn00972@heb.com)
- For creating EMS objects for StoreScanMaintenanceSubscriber
- release 1
