#! /bin/ksh
#################################################################
#
#
# Description:
#    Script used to create EMS Objects
#
#    Author: Jithesh Kumar
#
################################################################


HOME=${RPM_HOME}
FILES=${HOME}/files
SCRIPT=${HOME}/script
USER=admin
OBJECTS=${FILES}/objects.scr
OBJECTS_BAK=${FILES}/objects.scr.bak

## Run the environment script

if [ -f ${TIBCO_ROOT}/tibco.sh ] ; then
   . ${TIBCO_ROOT}/tibco.sh
fi

echo "Editing the $OBJECTS_BAK file for EMS Server Names"
CORP_NUM=`uname -n|tr [:upper:] [:lower:]|sed s/...// | cut -c3-5`
RC=$?
if [ ${RC} -ne 0 ] ; then
   echo "`date` Error while finding the store number: ${RC}"
   exit ${RC}
fi
case ${CORP_NUM} in
     890) ENV=DEV; EAIEMS=DICC08; EAIEMSURL=ldtibadm08a; EAIEMSPORT=7222;
     ;;
     860|891|894) ENV=CERT; EAIEMS=UEAI01; EAIEMSURL=drcapl0046720; EAIEMSPORT=7020;
     ;;
     *) ENV=PROD; EAIEMS=PEAI01; EAIEMSURL=lptibemseai01; EAIEMSPORT=7191;
     ;;
esac

## get user password
USER_PASSWD=`cat ${FILES}/${ENV}_password.txt`

# set the EMS details in the objects.scr file
sed 's/EAI_CENTRAL_EMS/'${EAIEMS}'/g' ${OBJECTS_BAK} | sed 's/EAICENTRALEMSURL/'${EAIEMSURL}'/g' | sed 's/CORP_NUM/'${CORP_NUM}'/g' | sed 's/EAIEMSPORT/'${EAIEMSPORT}'/g' | sed 's/USER_PASSWD/'${USER_PASSWD}'/g' | sed 's/EMSPASSWD/'${TIBCO_EMS_PASSWD}'/g' > ${OBJECTS}
RC=$?
if [ ${RC} -ne "0" ]; then
   echo "CRITICAL: Error while editing the objects fileErrNo: ${RC}, Exiting..."
   exit ${RC}
fi

echo "installing objects on server tcp://${EMS_URL}"
echo "INFO: Creating the EMS Objects"
${TIBEMSADMIN} -script ${OBJECTS} -server tcp://${EMS_URL} -user ${USER} -password ${TIBCO_EMS_PASSWD} -ignore
RC=$?
if [ ${RC} -ne 0 ] ; then
   echo "`date` CRITICAL: ERROR: The process encountered an error while trying to create EMS Objects: ${RC}"
   exit ${RC}
else
   echo "`date` INFO: EMS Object are created Successfully in tcp://${EMS_URL}"
   exit 0
fi
